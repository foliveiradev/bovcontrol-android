package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by felipe on 20/01/18.
 */
object NetworkServiceFactory {

    fun createTVShowAPI(baseUrl: String, httpClient: OkHttpClient) : TVShowAPI {
        val retrofit = retrofitBase(baseUrl, httpClient)
        return retrofit.create(TVShowAPI::class.java)
    }

    fun createImageAPI(baseUrl: String, httpClient: OkHttpClient) : ImageAPI {
        val retrofit = retrofitBase(baseUrl, httpClient)
        return retrofit.create(ImageAPI::class.java)
    }

    private fun retrofitBase(baseUrl: String, httpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}