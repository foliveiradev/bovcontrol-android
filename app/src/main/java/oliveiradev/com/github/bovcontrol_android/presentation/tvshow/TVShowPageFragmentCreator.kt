package oliveiradev.com.github.bovcontrol_android.presentation.tvshow
import android.content.Context
import android.support.v4.app.Fragment

/**
 * Created by felipe on 21/01/18.
 */
interface TVShowPageFragmentCreator {

    fun create(position: Int) : Fragment
    fun getContext() : Context
}