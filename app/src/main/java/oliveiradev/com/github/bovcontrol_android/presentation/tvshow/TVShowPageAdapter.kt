package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs.PageFragmentTitle

/**
 * Created by felipe on 20/01/18.
 */
class TVShowPageAdapter(
        fragmentManager: FragmentManager,
        private val pageCount: Int,
        private val creator: TVShowPageFragmentCreator) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment = creator.create(position)

    override fun getCount(): Int  = pageCount

    override fun getPageTitle(position: Int): CharSequence {
        return creator.getContext().getString((getItem(position) as PageFragmentTitle).titleResource)
    }
}