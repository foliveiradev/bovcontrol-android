package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by felipe on 20/01/18.
 */
data class ShowResponse(
        val title: String,
        val year: Int,
        val ids: IdsResponse,
        val overview: String,
        val rating: Float,
        val genres: List<String>,
        @SerializedName("first_aired") val firstAired: Date,
        val media: MediaResponse?
)