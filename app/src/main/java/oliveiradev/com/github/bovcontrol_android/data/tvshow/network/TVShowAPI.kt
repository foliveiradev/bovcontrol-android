package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by felipe on 20/01/18.
 */
interface TVShowAPI {

    @Headers("trakt-api-version: 2")
    @GET("shows/{id}")
    fun getShow(@Path("id") id: String,
                @Query("extended") extended: String = "full") : Single<ShowResponse>

    @Headers("trakt-api-version: 2")
    @GET("shows/{id}/seasons")
    fun getSeasons(@Path("id") id: String,
                @Query("extended") extended: String = "episodes") : Single<List<SeasonResponse>>
}