package oliveiradev.com.github.bovcontrol_android.data.tvshow

import io.reactivex.Single
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.MediaResponse
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.SeasonResponse
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.ShowResponse

/**
 * Created by felipe on 20/01/18.
 */
interface TVShowDataSource {

    fun getShow(id: String) : Single<ShowResponse>
    fun getShowImages(id: String) : Single<MediaResponse>
    fun getSeasons(id: String) : Single<List<SeasonResponse>>
}