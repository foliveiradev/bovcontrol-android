package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

import io.reactivex.functions.Function
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.SeasonResponse

/**
 * Created by felipe on 21/01/18.
 */
class SeasonDisplayableMapper(private val episodeDisplayableMapper: EpisodeDisplayableMapper) : Function<List<SeasonResponse>, List<SeasonUIModel>> {

    override fun apply(seasons: List<SeasonResponse>): List<SeasonUIModel> {
        if (seasons.isEmpty()) return listOf()

        return seasons.drop(1).map {
            SeasonUIModel(
                    "Season ${it.number}",
                    "${it.episodes.size} Episode(s)",
                    episodeDisplayableMapper.apply(it.episodes)
            )
        }
    }
}