package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

/**
 * Created by felipe on 21/01/18.
 */
data class EpisodeUIModel(
        val title: String
)