package oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs

/**
 * Created by felipe on 21/01/18.
 */
interface PageFragmentTitle {

    val titleResource: Int
}