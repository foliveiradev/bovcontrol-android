package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

import io.reactivex.functions.Function
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.SeasonResponse
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.ShowResponse
import java.util.*

/**
 * Created by felipe on 21/01/18.
 */
class TVShowDisplayableMapper(private val seasonDisplayableMapper: SeasonDisplayableMapper) : Function<Pair<ShowResponse, List<SeasonResponse>>, TVShowUIModel> {

    override fun apply(t: Pair<ShowResponse, List<SeasonResponse>>): TVShowUIModel {
        val (tvShowResponse, seasonsResponse) = t
        return TVShowUIModel(
                tvShowResponse.title,
                tvShowResponse.overview,
                tvShowResponse.genres.joinToString("|"),
                formatDateToMonthNYear(tvShowResponse.firstAired),
                seasonsResponse.size,
                tvShowResponse.rating,
                seasonDisplayableMapper.apply(seasonsResponse),
                tvShowResponse.media?.tvthumb?.firstOrNull()?.url,
                tvShowResponse.media?.tvbanner?.firstOrNull()?.url
        )
    }

    private fun formatDateToMonthNYear(date: Date): String {
        val calendar = Calendar.getInstance()
        calendar.time = date
        val month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
        val year = calendar.get(Calendar.YEAR)
        return "$month $year"
    }
}