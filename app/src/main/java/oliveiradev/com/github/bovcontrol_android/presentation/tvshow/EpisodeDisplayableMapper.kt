package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

import io.reactivex.functions.Function
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.EpisodeResponse

/**
 * Created by felipe on 21/01/18.
 */
class EpisodeDisplayableMapper : Function<List<EpisodeResponse>, List<EpisodeUIModel>> {

    override fun apply(episodes: List<EpisodeResponse>): List<EpisodeUIModel> {
        if (episodes.isEmpty()) return listOf()

        return episodes.map {
            EpisodeUIModel(it.title)
        }
    }
}