package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import oliveiradev.com.github.bovcontrol_android.domain.tvshow.TVShowInteractor

/**
 * Created by felipe on 20/01/18.
 */
class TVShowViewModelFactory(private val interactor: TVShowInteractor) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = TVShowViewModel(interactor) as T
}