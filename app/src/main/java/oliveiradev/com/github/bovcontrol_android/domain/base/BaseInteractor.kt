package oliveiradev.com.github.bovcontrol_android.domain.base

import io.reactivex.disposables.CompositeDisposable

/**
 * Created by felipe on 21/01/18.
 */
abstract class BaseInteractor {

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun dispose() {
        compositeDisposable.clear()
    }
}