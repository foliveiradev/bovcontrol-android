package oliveiradev.com.github.bovcontrol_android

import android.app.Application
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.squareup.leakcanary.LeakCanary
import oliveiradev.com.github.bovcontrol_android.di.Injection

/**
 * Created by felipe on 20/01/18.
 */
class App : Application(), KodeinAware {
    override val kodein: Kodein by Injection().graph

    override fun onCreate() {
        super.onCreate()
        LeakCanary.install(this)
    }
}