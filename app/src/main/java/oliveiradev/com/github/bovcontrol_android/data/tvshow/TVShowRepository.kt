package oliveiradev.com.github.bovcontrol_android.data.tvshow

import io.reactivex.Single
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.MediaResponse
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.SeasonResponse
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.ShowResponse

/**
 * Created by felipe on 20/01/18.
 */
class TVShowRepository(private val remoteDataSource: TVShowDataSource) : TVShowDataSource{

    override fun getShow(id: String): Single<ShowResponse> = remoteDataSource.getShow(id)
    override fun getShowImages(id: String): Single<MediaResponse> = remoteDataSource.getShowImages(id)
    override fun getSeasons(id: String): Single<List<SeasonResponse>> = remoteDataSource.getSeasons(id)
}