package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

/**
 * Created by felipe on 21/01/18.
 */
data class TVShowUIModel(
        val title: String,
        val overview: String,
        val genres: String,
        val startOf: String,
        val seasonAmount: Int,
        val rating: Float,
        val seasonUIModel: List<SeasonUIModel>,
        val thumb: String?,
        val banner: String?
)