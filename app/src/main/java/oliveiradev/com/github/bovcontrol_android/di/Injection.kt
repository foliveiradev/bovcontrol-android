package oliveiradev.com.github.bovcontrol_android.di

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import com.github.salomonbrys.kodein.*
import com.github.salomonbrys.kodein.android.androidActivityScope
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import oliveiradev.com.github.bovcontrol_android.BuildConfig
import oliveiradev.com.github.bovcontrol_android.data.base.RequestInterceptor
import oliveiradev.com.github.bovcontrol_android.data.tvshow.TVShowDataSource
import oliveiradev.com.github.bovcontrol_android.data.tvshow.TVShowRepository
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.ImageAPI
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.NetworkServiceFactory
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.TVShowAPI
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.TVShowRemoteDataSource
import oliveiradev.com.github.bovcontrol_android.domain.tvshow.TVShowInteractor
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.*

/**
 * Created by felipe on 20/01/18.
 */
class Injection {

    val graph = Kodein.lazy {

        bind<Interceptor>(LOGGER) with provider {
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        }

        bind<Interceptor>(NETWORK) with provider {
            RequestInterceptor()
        }

        bind<OkHttpClient>() with provider {
            OkHttpClient.Builder()
                    .addInterceptor(instance(LOGGER))
                    .addInterceptor(instance(NETWORK))
                    .build()
        }

        bind<TVShowAPI>() with provider {
            NetworkServiceFactory.createTVShowAPI(BuildConfig.TRAKT_BASE_URL, instance())
        }

        bind<ImageAPI>() with provider {
            NetworkServiceFactory.createImageAPI(BuildConfig.FANART_BASE_URL, instance())
        }

        bind<TVShowDataSource>(REMOTE) with provider {
            TVShowRemoteDataSource(instance(), instance())
        }

        bind<TVShowRepository>() with provider {
            TVShowRepository(instance(REMOTE))
        }

        bind<EpisodeDisplayableMapper>() with provider {
            EpisodeDisplayableMapper()
        }

        bind<SeasonDisplayableMapper>() with provider {
            SeasonDisplayableMapper(instance())
        }

        bind<TVShowDisplayableMapper>() with provider {
            TVShowDisplayableMapper(instance())
        }

        bind<TVShowInteractor>() with singleton {
            TVShowInteractor(instance(), instance())
        }

        bind<ViewModelProvider.Factory>() with provider {
            TVShowViewModelFactory(instance())
        }

        bind<TVShowViewModel>() with scopedSingleton(androidActivityScope) {
            ViewModelProviders
                    .of(it as FragmentActivity, instance())
                    .get(TVShowViewModel::class.java)
        }
    }

    companion object {

        private val REMOTE = "remote"
        private val LOCAL =  "local"
        private val LOGGER = "logger"
        private val NETWORK = "network"
    }
}