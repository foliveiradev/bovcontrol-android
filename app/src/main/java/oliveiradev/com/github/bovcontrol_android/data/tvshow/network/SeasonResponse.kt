package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

/**
 * Created by felipe on 21/01/18.
 */
data class SeasonResponse(
        val number: Int,
        val ids: IdsResponse,
        val episodes: List<EpisodeResponse>
)