package oliveiradev.com.github.bovcontrol_android.presentation.extensions

import android.app.Activity
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding

/**
 * Created by felipe on 21/01/18.
 */
fun <T : ViewDataBinding> Activity.bindActivity(activity: Activity, layoutId: Int): Lazy<T> {
    return lazy { DataBindingUtil.setContentView<T>(activity, layoutId) }
}