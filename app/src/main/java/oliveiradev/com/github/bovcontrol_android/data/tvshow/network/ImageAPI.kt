package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

import io.reactivex.Single
import oliveiradev.com.github.bovcontrol_android.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by felipe on 21/01/18.
 */
interface ImageAPI {

    @GET("{id}")
    fun getImages(
            @Path("id") id: String,
            @Query("api_key") apiKey: String = BuildConfig.FANART_API_KEY): Single<MediaResponse>
}