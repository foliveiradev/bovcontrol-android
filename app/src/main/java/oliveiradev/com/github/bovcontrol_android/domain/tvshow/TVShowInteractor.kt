package oliveiradev.com.github.bovcontrol_android.domain.tvshow

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiConsumer
import io.reactivex.schedulers.Schedulers
import oliveiradev.com.github.bovcontrol_android.data.tvshow.TVShowRepository
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.SeasonResponse
import oliveiradev.com.github.bovcontrol_android.data.tvshow.network.ShowResponse
import oliveiradev.com.github.bovcontrol_android.domain.base.BaseInteractor
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.TVShowDisplayableMapper
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.TVShowUIModel

/**
 * Created by felipe on 20/01/18.
 */
class TVShowInteractor(private val tvShowRepository: TVShowRepository,
                       private val mapper: TVShowDisplayableMapper) : BaseInteractor() {

    fun getShow(showId: String = "breaking-bad", consumer: BiConsumer<TVShowUIModel, Throwable>) {
        compositeDisposable.add(
                tvShowRepository.getShow(showId)
                        .flatMap { getShowImages(it) }
                        .flatMap { getSeasons(showId, it) }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(mapper)
                        .subscribe(consumer)
        )
    }

    private fun getShowImages(showResponse: ShowResponse) : Single<ShowResponse> {
        return tvShowRepository.getShowImages(showResponse.ids.tvdb)
                .map { showResponse.copy(media = it) }
    }

    private fun getSeasons(id: String,
                           showResponse: ShowResponse): Single<Pair<ShowResponse, List<SeasonResponse>>> {
        return tvShowRepository.getSeasons(id)
                .map { showResponse to it }
    }
}