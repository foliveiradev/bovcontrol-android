package oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import oliveiradev.com.github.bovcontrol_android.R
import oliveiradev.com.github.bovcontrol_android.databinding.FragmentDescriptionBinding
import oliveiradev.com.github.bovcontrol_android.databinding.FragmentDescriptionBinding.inflate
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.TVShowViewModel

/**
 * Created by felipe on 20/01/18.
 */
class DescriptionFragment : Fragment(), PageFragmentTitle {

    private lateinit var binding: FragmentDescriptionBinding
    private lateinit var viewModel: TVShowViewModel

    override val titleResource: Int by lazy { R.string.description_title }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val kodein = LazyKodein(appKodein)

        viewModel = kodein.value.with(activity).instance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = inflate(inflater, null)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.descriptionLiveData.observe(activity as LifecycleOwner, Observer { description ->
            binding.description = description
        })
    }

    companion object {

        fun newInstance() : DescriptionFragment {
            return DescriptionFragment()
        }
    }
}