package oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import oliveiradev.com.github.bovcontrol_android.R
import oliveiradev.com.github.bovcontrol_android.databinding.FragmentCastBinding

/**
 * Created by felipe on 20/01/18.
 */
class CastFragment : Fragment(), PageFragmentTitle{


    private lateinit var binding: FragmentCastBinding

    override val titleResource: Int by lazy { R.string.cast_title }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCastBinding.inflate(inflater, null)
        return binding.root
    }

    companion object {

        fun newInstance() : CastFragment {
            return CastFragment()
        }
    }
}