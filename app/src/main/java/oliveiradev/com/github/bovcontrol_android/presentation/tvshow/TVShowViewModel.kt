package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import io.reactivex.functions.BiConsumer
import oliveiradev.com.github.bovcontrol_android.domain.tvshow.TVShowInteractor

/**
 * Created by felipe on 20/01/18.
 */
class TVShowViewModel(private val interactor: TVShowInteractor) : ViewModel() {

    val uiModelObservable: ObservableField<TVShowUIModel> = ObservableField()
    val descriptionLiveData: MutableLiveData<String> = MutableLiveData()
    val seasonsLiveData: MutableLiveData<List<SeasonUIModel>> = MutableLiveData()

    fun getShow() {
        interactor.getShow(consumer = BiConsumer { uiModel, throwable ->
            if (throwable == null) {
                uiModelObservable.set(uiModel)
                descriptionLiveData.value = uiModel.overview
                seasonsLiveData.value = uiModel.seasonUIModel
            }
        })
    }

    override fun onCleared() {
        interactor.dispose()
        super.onCleared()
    }
}