package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

/**
 * Created by felipe on 1/22/18.
 */
data class ImageResponse(val url: String)