package oliveiradev.com.github.bovcontrol_android.presentation.tvshow

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import oliveiradev.com.github.bovcontrol_android.R
import oliveiradev.com.github.bovcontrol_android.databinding.ActivityTvShowBinding
import oliveiradev.com.github.bovcontrol_android.presentation.extensions.bindActivity
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs.CastFragment
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs.DescriptionFragment
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs.EpisodeFragment
import java.lang.IndexOutOfBoundsException

class TVShowActivity : AppCompatActivity(), TVShowPageFragmentCreator {

    private val kodein by lazy { LazyKodein(appKodein) }
    private val viewModel by kodein.with(this).instance<TVShowViewModel>()
    private val binding by bindActivity<ActivityTvShowBinding>(this, R.layout.activity_tv_show)
    private val pageAdapter by lazy {
        TVShowPageAdapter(supportFragmentManager, TAB_COUNT, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.executePendingBindings()
        binding.uiModel = viewModel.uiModelObservable
        setupView()
    }

    override fun onStart() {
        super.onStart()
        viewModel.getShow()
    }

    override fun create(position: Int): Fragment = when(position) {
        0 -> DescriptionFragment.newInstance()
        1 -> EpisodeFragment.newInstance()
        2 -> CastFragment.newInstance()
        else -> throw IndexOutOfBoundsException("Wrong tab position")
    }

    override fun getContext(): Context = this

    private fun setupView() {
        binding.scroll.isFillViewport = true
        setupTabs()
    }

    private fun setupTabs() {
        binding.viewPager.adapter = pageAdapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }

    companion object {

        private val TAB_COUNT = 3
    }
}
