package oliveiradev.com.github.bovcontrol_android.presentation.tvshow.tabs

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import iammert.com.expandablelib.ExpandableLayout
import iammert.com.expandablelib.Section
import oliveiradev.com.github.bovcontrol_android.R
import oliveiradev.com.github.bovcontrol_android.databinding.FragmentEpisodesBinding
import oliveiradev.com.github.bovcontrol_android.databinding.ItemEpisodeBinding
import oliveiradev.com.github.bovcontrol_android.databinding.ItemSeasonBinding
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.EpisodeUIModel
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.SeasonUIModel
import oliveiradev.com.github.bovcontrol_android.presentation.tvshow.TVShowViewModel

/**
 * Created by felipe on 20/01/18.
 */
class EpisodeFragment : Fragment(), PageFragmentTitle {

    private lateinit var binding: FragmentEpisodesBinding
    private lateinit var viewModel: TVShowViewModel

    override val titleResource: Int by lazy { R.string.episode_title }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val kodein = LazyKodein(appKodein)

        viewModel = kodein.value.with(activity).instance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentEpisodesBinding.inflate(inflater, null)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        viewModel.seasonsLiveData.observe(activity as LifecycleOwner, Observer { seasons ->
            seasons?.let {
                seasons.forEach { season -> binding.seasonsList.addSection(createSection(season)) }
            }
        })
    }

    private fun setupView() {
        binding.seasonsList.apply {
            setRenderer(renderSeasons())
            setCollapseListener<SeasonUIModel> { _, _, view ->
                view.findViewById<ImageView>(R.id.indicator).setImageResource(R.drawable.ic_keyboard_arrow_down)
            }

            setExpandListener<SeasonUIModel> { _, _, view ->
                view.findViewById<ImageView>(R.id.indicator).setImageResource(R.drawable.ic_keyboard_arrow_up)
            }
        }
    }

    private fun renderSeasons() : ExpandableLayout.Renderer<SeasonUIModel, EpisodeUIModel> {
        return object: ExpandableLayout.Renderer<SeasonUIModel, EpisodeUIModel> {
            override fun renderParent(view: View?, season: SeasonUIModel?, isExpanded: Boolean, parentPosition: Int) {
                val seasonBinding = DataBindingUtil.bind<ItemSeasonBinding>(view)
                seasonBinding.uiModel = season
                seasonBinding.indicator
                        .setImageResource(if (isExpanded) R.drawable.ic_keyboard_arrow_up else R.drawable.ic_keyboard_arrow_down)
            }

            override fun renderChild(view: View?, episode: EpisodeUIModel?, parentPosition: Int, childPosition: Int) {
                val episodeBinding = DataBindingUtil.bind<ItemEpisodeBinding>(view)
                episodeBinding.uiModel = episode
            }
        }
    }

    private fun createSection(season: SeasonUIModel): Section<SeasonUIModel, EpisodeUIModel> {
        val section: Section<SeasonUIModel, EpisodeUIModel> = Section()
        section.parent = season
        season.episodes.forEach {
            section.children.add(it)
        }

        return section
    }

    companion object {

        fun newInstance() : EpisodeFragment {
            return EpisodeFragment()
        }
    }
}