package oliveiradev.com.github.bovcontrol_android.presentation.binding_adapters

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso

/**
 * Created by felipe on 1/22/18.
 */
object ImageBindingAdapter {

    @JvmStatic
    @BindingAdapter("loadUrl")
    fun loadUrlWithPicasso(view: ImageView, url: String?) {
        Picasso
                .with(view.context)
                .load(if (url?.isBlank() == true) null else url)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .fit()
                .centerCrop()
                .into(view)
    }
}