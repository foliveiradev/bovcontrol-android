package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

import io.reactivex.Single
import oliveiradev.com.github.bovcontrol_android.data.tvshow.TVShowDataSource

/**
 * Created by felipe on 20/01/18.
 */
class TVShowRemoteDataSource(private val tvShowAPI: TVShowAPI, private val imageAPI: ImageAPI) : TVShowDataSource {

    override fun getShow(id: String): Single<ShowResponse> = tvShowAPI.getShow(id)
    override fun getShowImages(id: String): Single<MediaResponse> = imageAPI.getImages(id)
    override fun getSeasons(id: String): Single<List<SeasonResponse>> = tvShowAPI.getSeasons(id)
}