package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

/**
 * Created by felipe on 20/01/18.
 */
data class IdsResponse(
        val trakt: String,
        val slug: String,
        val tvdb: String,
        val imdb: String,
        val tmdb: String,
        val tvrage: String
)