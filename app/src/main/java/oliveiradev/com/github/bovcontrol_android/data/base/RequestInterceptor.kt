package oliveiradev.com.github.bovcontrol_android.data.base

import okhttp3.Interceptor
import okhttp3.Response
import oliveiradev.com.github.bovcontrol_android.BuildConfig

/**
 * Created by felipe on 21/01/18.
 */
class RequestInterceptor : Interceptor{

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder()

        requestBuilder.header("trakt-api-key", BuildConfig.TRAKT_API_KEY)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}