package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

/**
 * Created by felipe on 1/22/18.
 */
data class MediaResponse(
        val name: String,
        val tvthumb: List<ImageResponse>,
        val tvbanner: List<ImageResponse>
)