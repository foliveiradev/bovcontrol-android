package oliveiradev.com.github.bovcontrol_android.data.tvshow.network

/**
 * Created by felipe on 21/01/18.
 */
data class EpisodeResponse(
        val season: Int,
        val number: Int,
        val title: String,
        val ids: IdsResponse
)